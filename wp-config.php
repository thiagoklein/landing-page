<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'landing');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';_i3so++kD@7Aj}|#MF}-i))~!~y9L(zX8,n7zjwY}M`-N8F1RkgX6@%+H8w]wEf');
define('SECURE_AUTH_KEY',  'Pp8+?r[3Q11JM7gST)Z?.O>-r<I:1tI-vnk=2@&Ss5d9RkA>Ns%Na(ydYqw,Gy9r');
define('LOGGED_IN_KEY',    'h/f1%;I5{;3%=$BYb3 fJZ*GUsYdQBOVf+<DvRb<<?Wrg?M(7A6x[A,UQA}-c?e%');
define('NONCE_KEY',        'D*=)U!]Tj-Gc}0dXq3#>pHUW[IH<7(8:^R$H$I_Nf29}}:t%(<eNQ],8[Q(29X*L');
define('AUTH_SALT',        'ozglMmM$SdP&1!B*kC.z=LOwslyZR;LKpv]rz0k~2>-$2-NWYS@Dyfp#^Chss@se');
define('SECURE_AUTH_SALT', '!;`-o*PTn>*Pkw@HI{&x}>th, bnWM,=%gu~13k@4Uy}Bv9b/x.&-_sZ;NXg&+27');
define('LOGGED_IN_SALT',   'ymPq--Do=JKW|Uh=:fc6vTHeSe!J|p3JB%6UZdOxzUu-`^mC`g,n>8C2W}<)?WqQ');
define('NONCE_SALT',       'Q=<s8iiEi x+{zg=Cop@9+E_4%@`+#W1G|&g^KZa~DBhU-_AjP7#^T1aVCVkrVZ(');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD','direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

