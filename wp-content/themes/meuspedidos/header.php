<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package landing-teste
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css">

<link href='https://fonts.googleapis.com/css?family=Merriweather|Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">

		<div class="container">
			<div class="row">
				<div class="col-md-4">
				  <a href="http://blog.meuspedidos.com.br">
						<img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="O Blog do representante e gerente comercial" class="hidden-xs">
				    <h1 class="hidden-xs hidden-sm" id="slogan">O Blog do representante e gerente comercial</h1>
				  </a>
				</div>

				<nav id="site-navigation" class="col-md-6 main-navigation text-right" role="navigation">
					<?php wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu')); ?>
				</nav>

				<form role="search" method="get" id="searchform" class="searchform col-md-2" action="#">
	        <input type="text" value="" name="s" id="s" placeholder="Digite sua busca...">
	        <input type="submit" id="submit-search-top" value="">
				</form>

			</div>
		</div>

	</header>

	<div id="content" class="site-content">

		<div id="banner-conteudos" class="text-center">
			Conheça nosso novo ebook preparado especialmente para você! <br />
			<span>Gestão de Representantes</span> <br />
			<a href="ebooks/gestao-de-representantes">Baixar</a>
		</div>
