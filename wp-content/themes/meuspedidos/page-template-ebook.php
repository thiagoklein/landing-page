<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>eBook - Gestão de Representantes Comerciais</title>

  <meta name="description" content="[Ebook grátis] Gestão de representantes comerciais - Descubra como conquistar o comprometimento e aumentar o desempenho do seu time de vendas.">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="canonical" href="<?php bloginfo('template_url'); ?>/ebooks/gestao-de-representantes">
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/landing.css">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

  <meta name="twitter:card" content="summary">
  <meta name="twitter:image" content="https://rdstation-static.s3.amazonaws.com/images/landing_page/66859/offer-logo.png">
  <meta name="twitter:title" content="eBook - Gestão de Representantes Comerciais">
  <meta property="og:title" content="eBook - Gestão de Representantes Comerciais">
  <meta property="og:image" content="https://rdstation-static.s3.amazonaws.com/images/landing_page/66859/offer-logo.png">

</head>

<body class="landing-page">

  <header>
    <img src="<?= get_site_url(); ?>/wp-content/uploads/2016/01/logo_mp.png" width="189">
    <h1>eBook - Gestão de Representantes Comerciais</h1>
    <span>Conquiste o comprometimento e aumente o desempenho do seu time de vendas</span>
  </header>

  <section id="ebook-infos">
    <div class="container">
      <div class="box ebook-description">
        <h2>Descubra como ajudar seus representantes a venderem mais</h2>

        <p>Representantes comerciais são responsáveis pelo próprio desempenho, é verdade!</p>

        <p>Mas não tenha dúvidas: os resultados fora da curva em vendas só surgem quando existe uma sinergia e uma colaboração bilateral entre representante comercial e indústria.</p>

        <p>Neste ebook abordamos ações que você, gerente comercial da indústria ou distribuidora, pode fazer <b>para ajudar o seu representante comercial a vender mais.</b></p>
      </div>

      <div class="box ebook-image">
        <img src="<?= get_site_url(); ?>/wp-content/uploads/2016/01/ebook.png">
      </div>

      <div class="box ebook-form">
        <h2>Faça download grátis</h2>
        Basta preencher o formulário

        <form method="post" id="ebook-form" action="<?php echo admin_url('admin-ajax.php'); ?>">
          <input name="name" id="name" type="text" required placeholder="Nome (Obrigatório)" />
          <input name="email" id="email" type="email" required placeholder="E-mail (Obrigatório)" />
          <input name="phone" id="phone" type="text" required placeholder="Telefone (Obrigatório)" />
          <input type="hidden" name="action" value="send_ebook"/>
          <div>
            <input type="submit" value="Receber ebook">
            <img class="loader" src="<?php bloginfo('template_url'); ?>/img/loader.gif">
            <div class="ebook-ok">Ebook enviado para seu email, boa leitura!<br />Acompanhe outros conteúdos em nosso <a href="http://blog.meuspedidos.com.br">blog</a>!</div>
          </div>

        </form>
      </div>

    </div>
  </section>

  <section id="footer">
    <div class="container">
      <div id="phone">(47) 3481-8050</div>
      <div id="address">Av. Rolf Wiest 277, Auri Plaza Garten (7º andar) - Bom Retiro - Joinville SC - CEP 89223-005</div>
      <a href="https://meuspedidos.com.br/termos-de-uso/">Termos e Condições de Uso</a>
    </div>
  </section>

</body>
</html>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    $('#ebook-form').on('submit', function(e) {
      e.preventDefault();
      var $form = $(this);
      $("#ebook-form input[type='submit']").val("");
      $(".loader").show();
      $.post($form.attr('action'), $form.serialize(), function(data) {
        $(".loader").hide();
        $("#ebook-form input[type='submit']").val("Receber ebook");
        $("#ebook-form input").not("input[type='submit']").val("");
        $(".ebook-ok").fadeIn();
      }, 'json');
    });
  });
</script>