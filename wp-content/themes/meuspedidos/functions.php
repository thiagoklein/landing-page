<?php



add_action( 'wp_ajax_send_ebook', 'send_ebook' );
add_action( 'wp_ajax_nopriv_send_ebook', 'send_ebook' );

function send_ebook() {

  $name  = $_POST["name"];
  $email = $_POST["email"];
  $phone = $_POST["phone"];

  // tabela ainda é realidade para questões de compatibilidade :/
  $breakline = "<tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>"; // duas linhas

  $arquivo = "
    <html><body style='background-color: #fafafa;padding:0;text-align:center;margin:0;'>
    <table width='600' align='center' style='border: 1px solid #f0f0f0; background-color:white;' cellpadding=0 cellspacing=0>
    	$breakline
    	<tr><td align='center'><img src='http://localhost/landing/wp-content/uploads/email/email_logo.png' /></td></tr>
    	<tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr>
      <tr>
        <td align='center'><font style='font-family: Arial, sans-serif; color:#4E4E4E; font-size: 18px'>Obrigado por baixar nosso <br ><span style='color:#5493E0; font-size: 22px;'><b>eBook Gestão de Representantes Comerciais</b></span></font></td>
      </tr>
      $breakline
			<tr><td align='center'><img src='http://localhost/landing/wp-content/uploads/email/email_ebook.png' /></td></tr>
      $breakline
			<tr><td align='center'><a href='http://simplest-marketing.s3.amazonaws.com/ebooks/livro-gestao-representantes.pdf'><img src='http://localhost/landing/wp-content/uploads/email/email_botao.png' /></a></td></tr>
			$breakline
			<tr><td bgcolor='#F2F2F2'>
			<table width='100%' style='background-color:#F2F2F2;' cellpadding=0 cellspacing=0>
				<tr height='80' valign='middle'>
					<td width='35'>&nbsp;</td>
					<td width='265' align='left'>
						<a href='https://www.facebook.com/meuspedidos.com.br'><img src='http://localhost/landing/wp-content/uploads/email/email_facebook.png' /></a>
						<a href='https://www.linkedin.com/company/meuspedidos'><img src='http://localhost/landing/wp-content/uploads/email/email_linkedin.png' /></a>
					</td>
					<td width='265' align='right'>
						<a href='https://meuspedidos.com.br'><img src='http://localhost/landing/wp-content/uploads/email/email_site.png' /></a>
					</td>
					<td width='35'>&nbsp;</td>
				</tr>
			</table>
			</td></tr>
    </table></body></html>";

    $to          = "klein.thiago@gmail.com";
    $subject     = "[Ebook grátis] - Gestão de representantes comerciais";
    $message     = $arquivo;
    $headers     = "";
    $attachments = array(WP_CONTENT_DIR.'/uploads/ebooks/livro-gestao-representantes.pdf');
		add_filter( 'wp_mail_content_type', 'set_html_content_type' );

    wp_mail($to, $subject, $message, $headers, $attachments);

		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

    $response = true;

    exit(json_encode($response));
}

function set_html_content_type() { return 'text/html'; }


/**
 * landing-teste functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package landing-teste
 */

if ( ! function_exists( 'landing_teste_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function landing_teste_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on landing-teste, use a find and replace
	 * to change 'landing-teste' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'landing-teste', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'landing-teste' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'landing_teste_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'landing_teste_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function landing_teste_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'landing_teste_content_width', 640 );
}
add_action( 'after_setup_theme', 'landing_teste_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function landing_teste_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'landing-teste' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'landing_teste_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function landing_teste_scripts() {
	wp_enqueue_style( 'landing-teste-style', get_stylesheet_uri() );

	wp_enqueue_script( 'landing-teste-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'landing-teste-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'landing_teste_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


function catch_that_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches[1][0];

  if(empty($first_img)) {
    $first_img = "/path/to/default.png";
  }
  return $first_img;
}
