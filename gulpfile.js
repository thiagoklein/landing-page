var gulp = require('gulp');

var moveto = 'wp-content/themes/meuspedidos/';

gulp.task('default', function() {
  gulp.src('bower_components/jquery/dist/jquery.min.js').pipe(gulp.dest(moveto+"/js/"));
  gulp.src('bower_components/bootstrap/dist/js/bootstrap.min.js').pipe(gulp.dest(moveto+"/js/"));
  gulp.src('bower_components/bootstrap/dist/css/bootstrap.min.css').pipe(gulp.dest(moveto+"/css/"));
  gulp.src('bower_components/bootstrap/dist/fonts/*').pipe(gulp.dest(moveto+"/fonts/"));
});

